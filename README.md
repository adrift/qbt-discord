# qbt-discord

A Discord bot assistant to interface with qBittorrent via the [qBittorrent API](https://github.com/qbittorrent/qBittorrent/wiki/WebUI-API-(qBittorrent-4.1) thanks to the [qbittorrent-api](https://pypi.org/project/qbittorrent-api/) Python package.

## Configurables:

All configurables can be overridden via environment variables. If an environment variable is not found, the default value will be used.

| Key            | Default Value | Required | Notes                                                                                                                            |   |   |
|----------------|---------------|----------|----------------------------------------------------------------------------------------------------------------------------------|---|---|
| DISCORD_TOKEN  | `None`        | ✅        | Standard Discord bot token. Be sure to invite this bot to your server before running                                             |   |   |
| QBT_WEBUI      | `localhost`   |          | The hostname or IP to reach the qBittorrent instance by                                                                          |   |   |
| QBT_PORT       | `8080`        |          | The port to contact the qBittorrent instance upon                                                                                |   |   |
| QBT_NOAUTH     | `False`       |          | qBittorrent can be configured to allow certain IPs and ranges to access it with no auth                                          |   |   |
| QBT_USER       | `admin`       |          | Unless `QBT_NOAUTH`, the user to log into the qBittorrent instance with. Uses default qbt creds.                                 |   |   |
| QBT_PASS       | `adminadmin`  |          | Unless `QBT_NOAUTH`, the pass to log into the qBittorrent instance with. Uses default qbt creds.                                 |   |   |
| QBT_VERIFY_SSL | `False`       |          | If you are using valid SSL/TLS certificates from a public CA or properly have your self signed stuff trusted, set this to `True` |   |   |

## Quick Start

```
DISCORD_TOKEN="hunter2"
python main.py
```
