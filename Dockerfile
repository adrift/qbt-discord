FROM python:3

ENV WORKDIR=/opt/app
COPY . $WORKDIR

RUN pip install -r $WORKDIR/requirements.txt

CMD [ 'main.py' ]
