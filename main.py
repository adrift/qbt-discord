#!/bin/python3

import os
import asyncio
import qbittorrentapi
import discord

# important environment variables
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
QBT_WEBUI = os.getenv('QBT_WEBUI', 'localhost')
QBT_PORT = os.getenv('QBT_PORT', 8080)
QBT_NOAUTH = os.getenv('QBT_NOAUTH', False)
QBT_USER = os.getenv('QBT_USER', 'admin')
QBT_PASS = os.getenv('QBT_PASS', 'adminadmin')
QBT_VERIFY_SSL = os.getenv('QBT_VERIFY_SSL', False)

qbt_client = qbittorrentapi.Client(host=QBT_WEBUI, port=QBT_PORT, username=QBT_USER, password=QBT_PASS)

discord_client = discord.Client()

async def time_s_to_h(seconds):
    return seconds / 3600

async def write_log(message):
    print(
        f'[{message.created_at}] {message.id} {message.guild} #{message.channel} {message.author}: "{message.content}"'
    )

@discord_client.event
async def on_ready():
    print(f'{discord_client.user} has connected to Discord!')
    
    guild = discord.utils.get(discord_client.guilds)
    print(
        f'{discord_client.user} is connected to guilds:\n'
        f'◌ "{guild.name}" (id: {guild.id})'
    )

@discord_client.event
async def on_message(message):
    if message.author == discord_client.user:
        return
    
    if not discord_client.user.id in message.raw_mentions:
        return

    #
    # search!
    # a lot of it looks like shit thanks to the underlying qbt api being shit
    # the propagative property of shit or something
    #
    if 'search' in message.content:
        await write_log(message)
        await message.add_reaction('⌛')

        query = message.content.split('search ')[1]
        results = ""
        for torrent in qbt_client.torrents_info(): # qbt doesn't support actually querying directly
            if query.lower() in torrent.name.lower(): # convert all to lower to match more greedily
                # this process number handling should be moved to a function
                if isinstance(torrent.progress,int):
                    progress = f'{torrent.progress}%'
                    if torrent.progress is 1:
                        header_emoji = "✅"
                        progress = "100%"
                if isinstance(torrent.progress,float): 
                    progress_percent = torrent.progress * 100
                    progress = f'{round(progress_percent, 2)}%, {time_s_to_h(torrent.eta)}h remaining'
                    if torrent.downspeed is not 0:
                        header_emoji = "⬇️"

                results += (f'{torrent.hash[-6:]}: {torrent.name} | {torrent.state} | {progress}\n')

                # this doesnt work yet?
                if results == "":
                    results = "Query returned no results, try again. It's possible there exist no torrent even, if we couldn't originally find something"
        
        try:
            await message.channel.send(f'{results}')
            await message.add_reaction('✔️')
        except Exception as err:
            await message.add_reaction('❌')
            await message.channel.send(
                '💣! Review your input, as well as the broadness of your query and try again. Error:\n'
                f'```{err}```'
            )

    if 'usage' in message.content:
        await message.channel.send(
            'adrift qbittorrent bot on standby!\n'
            'this bot is designed to help you get info on media you\'ve requested, but hasn\'t been "Imported" yet\n\n'
            '**COMMANDS**\n'
            '`@adrift-qbt search STRING`\n'
            'Best to keep simple and single-worded. Case-insensitive\n'
            'Example: `@adrift-qbt search pets`\n'
        )

discord_client.run(DISCORD_TOKEN)
